# HTTP Server
This project is a simple HTTP server implementation which servers files from a user specified directory.
It handles **GET** and **HEAD** requests, creates **ETags** for requested resources and handles **If-Match**, 
**If-None-Match** and **If-Modified-Since** request headers.
 
## Index
* [RFC Obedience](#rfc-obedience)
* [Concurrency](#concurrency) 
  * [IO vs NIO](#io-vs-nio)
  * [File Locks](#file-locks)
  * [Starvation/Fairness](#starvationfairness)
* [Design Pattern](#design-pattern)
  * [Builder Pattern](#builder-pattern)
  * [Chain of Responsibility](#chain-of-responsibility)
* [Sequence Diagram](#sequence-diagram)
* [Code Quality](#code-quality)
  * [Code Style](#code-style)
  * [Documentation](#documentation)
  * [Encapsulation](#encapsulation)
  * [Exception Handling](#exception-handling)
  * [Logging](#logging)
  * [Testing](#testing)
* [Challenges](#challanges)
  * [ETag generation](#etag-generation)
* [Performance](#performance)
  * [ETag Caching](#etag-caching)
  * [Stress Test](#stress-test)
* [Build It!](#build-it)
 
### RFC Obedience
To ensure that the server is supported by a wide range of clients and caches, following rules defined in the following
 RFCs is important:
* [RFC 2616 - HTTP/1.1](#https://tools.ietf.org/html/rfc2616)
* [RFC 7232 - Precondition Header Fields](#https://tools.ietf.org/html/rfc7232)
* [RFC 1123 - Time Format](#https://tools.ietf.org/html/rfc1123)

The entire parsing of HTTP Reqeusts are described there, what behaviours is expected by the client, how Reqeust Headers
should be interpreted and so on.


### Concurrency
As the server might be requested by more than one client at the time, concurrency and thread safety is a crucial element
of the implementation. It's important to constantly keep the program flow in mind and identify potential race conditions,
deadlocks and resource leaks. It's wise to avoid concurrency issues from the beginning since they are very trickty to 
troubleshoot. That is also the reason why I wen't for a simpler implementation rather than something complicated. More
on that in [IO vs NIO](#io-vs-nio).

#### IO vs NIO
A big part of the design consideration has been the choice between a IO and NIO implementation of the server.
This [article](#http://tutorials.jenkov.com/java-nio/nio-vs-io.html) describes the key differences really well.

My goal was to aim for a simple implementation which has one main thread that listens for incoming requests, accepts
them and creates a dedicated HttpHandler thread that takes care of the whole IO in itself. This prevents issues with
shared mutable resources and resulting race conditions. Everything happens inside one Thread and resources never leave 
the thread scope. Also reading from Streams instead of Buffers felt like the better solution for an HTTP server.
Especially if big files are being transferred.
```
                                +-------------+     +---------+
                            +-->| Connection  +---->| Thread  |
                            |   +-------------+     +---------+
+---------+    +---------+  |
| Server  |    |         |  |   +-------------+     +---------+
| Socket  +--->| Thread  +--+-->| Connection  +---->| Thread  |
|         |    |         |  |   +-------------+     +---------+
+---------+    +---------+  |
                            |   +-------------+     +---------+
                            +-->| Connection  +---->| Thread  |
                                +-------------+     +---------+
```
The only shared resource is the cached ETag (See [ETag Caching](#etag-caching)) and the served file itself.
Multiple threads reading from a shared resource (like the requested file) will not cause any concurrency issues, 
requests that apply changes however are a different story. 

#### File locks
For the current implementation which does not support PUT, POST, DELETE or other requests that apply changes to the
target resource, file locks are less of an issues. However it does make sense to think about a way to avoid unwanted
effects for future implementations that might come.

The basic idea is that each resource (file in our case) can be read by multiple threads without causing any problems.
As soon as the first requests tries to change a resource, we have to make sure that all other treads are waiting until
the writing thread as finished. After that other reading threads can resume safely.

The java.nio packages offer ways of solving file lock problems.

Invalidating the ETag cache once a write operation is done is a crucial part, that will cause terrible side-effects if
not done properly. Future conditional requests might get an cached value form the ETag cache and react in ways not 
feasible understanding.

#### Starvation/Fairness
In the [File locks](#file-locks) part, we've talked about the general idea of implementing file locks.
Another thing to consider at this point is thread Starvation and Fairness. Let's imagine  that a writing thread has a
higher priority over reading threads. As long as there are significantly more reading threads than writing threads, the 
likeliness of a reading thread starving is very little. However as soon as writing threads start to become the majority,
reading threads will have to wait until all writing threads are done. This will increase the chances of a reading thread
die of starvation substantially.

A possible solution is to implement a fairness lock mechanism that equally balances out the file access between reading 
and writing threads so that every thread will eventually be executed.

### Design Pattern
Another part of the implementation of the HTTP server was to decide which design patterns make the most sense in terms
of being beneficial to the solution and creating coherent and understandable code.  

#### Builder Pattern
One big part of the implementation is the use of Builder pattern.
Since each request is being gradually created, the use of builders are prefect for that scenario! Parsed
values can be sequentially added to the builder step by step and the actual Object creation is happening when the 
builder "builds" the desired object. When building we can take care of validating the input, checking if all mandatory 
fields have been sat and so on.

```
+---------+                                    +-------+    +--------------+
| Request +----------------------------------->+ BUILD +--->+ Http Request |
| Builder |   ^          ^     ^      ^        +-------+    +--------------+
+---------+   |          |     |      |
              |          |     |      +---------------------+
              |          |     +--------+                   |
              |          |              |                   |
   +----------+--+   +---+--------+   +-+----------+   +----+------------+
   | Add Version |   | Add Header |   | Add Method |   | Add Request URI |
   +-------------+   +------------+   +------------+   +-----------------+

```
The same pattern is applied to the HttpResponseBuilder. 

#### Chain of Responsibility
To keep the code clean, readable and understandable I've applied the Chain of Responsibility pattern for request
handling. Once a request has been parsed from the clients input, the HttpHandler thread is handing over the HttpRequest,
InputStream and OutputStream into the first link of the responsibility chain.

```
Write Output
Break Chain
     ^
     |                
     |     +---------------------------+
     |     | Favicon Handler           |
     |     +---------------------------+----+
     |                                      | Handle Next
     |                                      |
     |     +---------------------------+<---+
     |     | Thread Pause Handler      |
     +-----+---------------------------+----+
     |                                      | Handle Next
     |                                      |
     |     +---------------------------+<---+
     |     | 404 Handler               |
     +-----+---------------------------+----+
     |                                      | Handle Next
     |                                      |
     |     +---------------------------+<---+
     |     | If-Modified-Since Handler |
     +-----+---------------------------+----+
     |                                      | Handle Next
     |                                      |
     |     +---------------------------+<---+
     |     | If-Match Handler          |
     +-----+---------------------------+----+
     |                                      | Handle Next
     |                                      |
     |     +---------------------------+<---+
     |     | If-None-Match Handler     |
     +-----+---------------------------+----+
     |                                      | Handle Next
     |                                      |
     |     +---------------------------+<---+
     |     | MethodReqeust Handler     |
     +-----+---------------------------+
```

Each Handler is responsible for just one thing at a time, it does its job then either breaks the chain or calls the 
next handler in line. Lets take the Favicon Handler for example. The condition for this handler is that the request 
method is **GET** and that the reqeust URI is **/favicon.ico**. If the condition is fulfilled, the handler creates a new
ResponseBuilder, fills all necessary fields for the response (including the favicon.ico) then builds the response and 
sends it (to the OutputStream of the current connection). After that it breaks the chain.

If the condition for the handler weren't fulfilled, it would have just called the next handler in line and passed the 
HttpRequest, root directory and output stream over. 

Another way of breaking the chain is to throw an HttpException but more on that in [Exception Handling](#exception-handling). 

### Sequence Diagram
The following sequence diagram describes a sequence between client and server.
```
+----------+              +----------+               +-------------+
|  Client  | Send Request |  Server  |               | HttpHandler |
|          +-------------->          |               |             |
|          |              |          | Create Thread |             |
|          |              |          +--------------->             |
|          |              |          |               |             | Parse Reqeust      +-------------------+
|          |              |          |               |             +--------------------> HttpRequestParser |
|          |              |          |               |             |                    |                   |
|          |              |          |               |             | Return HttpRequest |                   |
|          |              |          |               |             <--------------------+                   |
|          |              |          |               |             |                    |                   |
|          |              |          |               |             |                    +-------------------+
|          |              |          |               |             |
|          |              |          |               |             | Start Chain        +-------------------+
|          |              |          |               |             +--------------------> Chain of          |
|          |              |          |               |             |                    | Responsibility    |
|          |              |          |               |             | Send Response      |                   |
|          <----------------------------------------------------------------------------+                   |
|          |              |          |               |             |                    +-------------------+
|          |              |          |               |             |
|          |              |          |               |             |
|          |              |          |               |             |
+----------+              +----------+               +-------------+

```
### Code Quality

#### Code Style
I've decided on the google code style for this project since it is well known and I wanted to try something new.
The 2 space indentation really compresses the width of the code, which I really started to like. In combination with 
the intellij checkstyle and google-java-format plugin, the code is always being inspected, which helps a lot.

#### Documentation
Nothing fancy here, All classes except of test classes are fully documented. I put emphasis of linking RFC documentation
via `{@link}` tag to the Javadoc, so that other developers can not only see the intention but also read through the 
documents upon which the implememtation is based.

#### Encapsulation
While writing this HTTP server, code encapsulation and separation of concern was always a focus to me.
I've tried to keep every method effectively below 50 lines of code.
This not only makes the code more readable, it also helps greatly with the codes testability since methods have a fairly
small footprint.

#### Exception Handling
Exception Handling is separated into 3 Types:
* HttpExceptions
* HttpPraseExceptions
* HttpResponseException

##### HttpExceptions
This type of exception is meant to completely break out of the responsibility chain, if something unusual happens e.g 
if parts of the code are not implemented they are secured by throwing an HttpException with a 501 (Not Implemented)
status code attached to it. These type of exception is catched in the main HttpHandler thread and then handed over to
the HttpExceptionHandler which renders a HTTP page based on the exceptions HttpStatusCode. The reason it's handled this 
way, is because the HttpHandler thread has still access to the connections OutputStream at this point. 

#### Logging
Logging is being done via Log4j2, it's set up to log to the console. If logging to a logfile is required, the `log4j2.xml`
file has to be adjusted accordingly. 

#### Testing
During the development process, testability has always been a main focus.
All testing is done with Unit tests, all necessary parts of the implementation are being covered by unit tests.
The good encapsulation comes in quite handy, every input, output can be parameterized and/or properly asserted.

One part that is sadly missing, is a stress test to see how the implementation behaves under heavy load.
See: [Stress Test](#stress-test).

Integration tests didn't seem necessary since the test coverage by the Unit-Tests are sufficient enough. 

### Challanges

#### ETag generation
ETag generation turned out to be not as trivial as thought at the beginning.
One goal was to generate reliable and fast hash values to be used for strong validation. Hash collision was less of a
concern to me since I've hashed the entire files with a CRC32C algorithm. To me, that was sufficient enough, since this 
particular hash of a requested resource must not be unique among all available resources but only the one reqeusted.

The weak validation is a totally different horse. To enable weak validation a server side algorithm has to be developed 
which is capable of distinguishing semantic changes to the file and decide whether the meaning of the file has been 
changed or not. Based on this, a client can request a weak validation for a specified resource.

That algorithm was out of scope in this implementation, therefore I've only allowed strong bit to bit validation 
represented by the CRC32C hash.

### Performance
#### Stress Test
I've planned stress testing the HTTP server with JMeter and see how the current implementation performs under heavy load.
I wanted to compare the current Multi-Threading implementation with a Thread-Pool implementation and see how the 
performance is affected. Sadly this was out of scope for the given timeframe.

#### ETag Caching
To boost the server response a notch, a thread safe ETag cache has been implemented.
This cache is disabled by default because it is unknown to me, how the files on the root dir are being manipulated.
If all changes to the files are happening via http server, the cache could be safely enabled.   

# Build it!

## Prerequisites
* Openjdk 11

## Build
`./gradlew clean build`

## Start Server
`java -jar ./build/libs/httpserver-all.jar`

## Docker
Build: `docker build -t httpserver .`

Run: `docker run -p <host_port>:8080 -v <root_directory>:/rootdir httpserver`

Run Example `docker run -p 8080:8080 -v /home/user/pictures:/rootdir httpserver`
