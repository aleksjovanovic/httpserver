FROM openjdk:11
WORKDIR /opt/httpserver
COPY build/libs/httpserver-all.jar /opt/httpserver/httpserver-all.jar
#COPY rootdir /opt/httpserver/rootdir
CMD ["java", "-jar", "/opt/httpserver/httpserver-all.jar", "-r", "/rootdir"]