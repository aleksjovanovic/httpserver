package de.monoceros.httpserver.constant;

/**
 * Enumeration of General Header Fields defined in RFC 2616.
 *
 * @see <a href="https://tools.ietf.org/html/rfc2616#section-4.5">RFC 2616 - General Header Fields</a>
 */
public enum HttpHeader {
  ETAG("ETag"),
  CONTENT_LENGTH("Content-Length"),
  CONTENT_LOCATION("Content-Location"),
  LAST_MODIFIED("Last-Modified"),
  IF_MATCH("If-Match"),
  IF_NONE_MATCH("If-None-Match"),
  IF_MODIFIED_SINCE("If-Modified-Since"),
  THREAD_PAUSE("Thread-Pause"), // Custom header to test concurrency
  DATE("Date");

  private String name;

  HttpHeader(String name) {
    this.name = name;
  }

  public String getName() {
    return this.name;
  }
}
