package de.monoceros.httpserver.constant;

/**
 * Enumeration of available HTTP 1.1 methods defined in RFC 2616.
 *
 * @see <a href="https://tools.ietf.org/html/rfc2616#section-5.1.1">RFC 2616 - Method</a>
 */
public enum HttpMethod {
  GET,
  HEAD,
  POST,
  OPTIONS,
  PUT,
  DELETE,
  TRACE,
  CONNECT
}
