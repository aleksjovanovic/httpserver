package de.monoceros.httpserver.constant;

/** Class which provides all sorts of HTTP specific tokens */
public class HttpToken {
  public static final char CARRIAGE_RETURN = (char) 0x0D;
  public static final char LINE_FEED = (char) 0x0A;
  public static final String CARRIAGE_RETURN_LINE_FEED =
      String.valueOf(CARRIAGE_RETURN) + LINE_FEED;
  public static final String SPACE = " ";
  public static final String SUPPORTED_HTTP_VERSION = "HTTP/1.1";
}
