package de.monoceros.httpserver;

import de.monoceros.httpserver.server.ETagCache;
import de.monoceros.httpserver.server.HttpServer;

import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.NotDirectoryException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.UnrecognizedOptionException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/** Main class which creates the HttpServer instance. */
public class Launcher {

  private static final Logger logger = LogManager.getLogger(Launcher.class);

  /**
   * Main Method which parses given args and starts the HttpServer accordingly.
   *
   * @param args Arguments provided by the cmd line
   */
  public static void main(String[] args) {
    // Setup command line parser
    Options options =
        new Options()
            .addOption("p", "port", true, "Specifies the server port")
            .addOption("c", "cache-enabled", true, "Specifies whether the ETag caching is enabled or not")
            .addOption("r", "rootdir", true, "Specifies the servers root directory");
    try {
      DefaultParser defaultParser = new DefaultParser();
      CommandLine parse = defaultParser.parse(options, args);

      // Parse given command lines
      int port = Integer.parseInt(parse.getOptionValue("p", "8080"));
      Path rootDir = Paths.get(parse.getOptionValue("r", "./rootdir"));
      boolean cacheEnabled = Boolean.parseBoolean(parse.getOptionValue("c", "false"));

      // Check if root dir exists
      if (!Files.exists(rootDir)) {
        throw new FileNotFoundException(
            "Specified root directory does not exist: " + rootDir.toString());
      } else if (!Files.isDirectory(rootDir)) {
        throw new NotDirectoryException(
            "Specified root directory is not a directory: " + rootDir.toString());
      }

      // Enable ETag cache
      if (cacheEnabled) {
        ETagCache.getInstance().enableCache();
      }

      // Start Server
      HttpServer httpServer = new HttpServer(port, rootDir);
      httpServer.run();
    } catch (UnrecognizedOptionException uoe) {
      // Print usage
      logger.error(uoe.getMessage());
      HelpFormatter formatter = new HelpFormatter();
      formatter.printHelp("java -jar httpserver", options, true);
    } catch (Exception e) {
      logger.fatal("Initializing Http Server has failed", e);
    }
  }
}
