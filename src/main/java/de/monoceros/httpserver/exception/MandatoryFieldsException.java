package de.monoceros.httpserver.exception;

public class MandatoryFieldsException extends HttpParseException {
  public MandatoryFieldsException(String message) {
    super(message);
  }
}
