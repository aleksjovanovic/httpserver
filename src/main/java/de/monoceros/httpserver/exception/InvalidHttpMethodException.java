package de.monoceros.httpserver.exception;

public class InvalidHttpMethodException extends HttpParseException {
  public InvalidHttpMethodException(String message) {
    super(message);
  }
}
