package de.monoceros.httpserver.exception;

import de.monoceros.httpserver.constant.HttpStatusCode;

public class HttpException extends Exception {
  private HttpStatusCode httpStatusCode;

  public HttpException(HttpStatusCode httpStatusCode, Throwable cause) {
    super(cause);
    this.httpStatusCode = httpStatusCode;
  }

  public HttpException(HttpStatusCode httpStatusCode) {
    this.httpStatusCode = httpStatusCode;
  }

  public HttpStatusCode getHttpStatusCode() {
    return httpStatusCode;
  }
}
