package de.monoceros.httpserver.exception;

public class UnsupportedHttpVersionException extends HttpParseException {
  public UnsupportedHttpVersionException(String message) {
    super(message);
  }
}
