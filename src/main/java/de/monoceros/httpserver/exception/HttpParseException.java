package de.monoceros.httpserver.exception;

public class HttpParseException extends Exception {
  public HttpParseException(String message) {
    super(message);
  }
}
