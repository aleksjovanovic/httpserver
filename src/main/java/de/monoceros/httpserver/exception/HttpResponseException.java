package de.monoceros.httpserver.exception;

public class HttpResponseException extends Exception {
  public HttpResponseException(String message) {
    super(message);
  }

  public HttpResponseException(String message, Throwable cause) {
    super(message, cause);
  }
}
