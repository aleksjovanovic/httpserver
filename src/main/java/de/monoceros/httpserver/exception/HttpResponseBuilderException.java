package de.monoceros.httpserver.exception;

public class HttpResponseBuilderException extends HttpResponseException {
  public HttpResponseBuilderException(String message) {
    super(message);
  }
}
