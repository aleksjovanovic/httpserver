package de.monoceros.httpserver.server;

import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;
import com.google.common.io.Files;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.ConcurrentHashMap;

/** Singleton caching class which keeps generated ETags in Memory. */
public class ETagCache {

  private static final Logger logger = LogManager.getLogger(ETagCache.class);

  private static ETagCache instance;
  private ConcurrentHashMap<Path, String> cache;
  private boolean enableCache = false;

  private ETagCache() {
    cache = new ConcurrentHashMap<>();
  }

  /**
   * Get singleton {@link ETagCache} instance.
   *
   * @return {@link ETagCache}
   */
  public static ETagCache getInstance() {
    if (instance == null) {
      instance = new ETagCache();
    }
    return instance;
  }

  /** Disables the caching of ETags. */
  public synchronized void disableCache() {
    logger.info("Disabling ETag cache.");
    this.enableCache = false;
  }

  /** Enables the caching of ETags. */
  public synchronized void enableCache() {
    logger.info("Enabling ETag cache.");
    this.enableCache = true;
  }

  /** Check if cache is enabled. */
  public synchronized boolean isCacheEnabled() {
    return this.enableCache;
  }

  /**
   * <b>If the cache is enabled:</b>
   *
   * <p>Gets an ETag from cache, if no ETag has been found in the case, this method will generate a
   * new ETag for the requested resource and put it in the cache.
   *
   * <p><b>if the cache is disabled:</b>
   *
   * <p>Generate and return ETag right away without caching it
   *
   * @param path path to requested resource
   * @return ETag (Hash) for requested resource
   */
  public String getETag(Path path) {
    if (isCacheEnabled()) {
      String etag = cache.get(path);
      if (etag == null) {
        etag = generateETag(path);
        update(path, etag);
      }
      return etag;
    }
    return generateETag(path);
  }

  public void remove(Path path) {
    cache.remove(path);
  }

  public void update(Path path, String md5) {
    cache.put(path, md5);
  }

  /**
   * Strong compare algorithm for ETags.
   *
   * @param tag1 tag to compare
   * @param tag2 tag to compare with
   * @return true if strongEqual false if not
   * @see <a href="https://tools.ietf.org/html/rfc7232#section-2.3.2">RFC 7232 - Comparison</a>
   */
  public static boolean strongCompare(String tag1, String tag2) {
    if (isWeak(tag1) || isWeak(tag2)) {
      return false;
    }
    return tag1.equals(tag2);
  }

  /**
   * Weak compare algorithm for ETags.
   *
   * @param tag1 tag to compare
   * @param tag2 tag to compare with
   * @return true if strongEqual false if not
   * @see <a href="https://tools.ietf.org/html/rfc7232#section-2.3.2">RFC 7232 - Comparison</a>
   */
  public static boolean weakCompare(String tag1, String tag2) {
    return weaken(tag1).equals(weaken(tag2));
  }

  /**
   * Check if given tag is a weak one.
   *
   * @param tag tag to check
   * @return true if tag has W/ prefix false otherwise
   */
  private static boolean isWeak(String tag) {
    return tag.indexOf("W/") == 0;
  }

  private static String weaken(String tag) {
    if (isWeak(tag)) {
      return tag;
    }
    return "W/" + tag;
  }

  /**
   * Generates an ETag based on CRC32C hashing algorithm.
   *
   * @see <a href="https://tools.ietf.org/html/rfc7232#section-2.3">RFC 7232 - ETag</a>
   * @param path file to generate the ETag from
   * @return quoted-string ETag for given {@link Path} file described in RFC 7232
   */
  private String generateETag(Path path) {
    try {
      HashCode hash = Files.asByteSource(path.toFile()).hash(Hashing.crc32c());
      return "\"" + hash.toString() + "\"";
    } catch (IOException ioe) {
      ioe.printStackTrace();
    }
    return null;
  }
}
