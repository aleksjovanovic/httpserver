package de.monoceros.httpserver.server.handler;

import de.monoceros.httpserver.constant.HttpStatusCode;
import de.monoceros.httpserver.exception.HttpException;
import de.monoceros.httpserver.exception.HttpResponseException;

import java.io.OutputStream;

import de.monoceros.httpserver.server.HttpResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class that handles {@link HttpException} and renders HTML response according to the {@link
 * de.monoceros.httpserver.constant.HttpStatusCode}.
 */
public class HttpExceptionHandler {

  private static final Logger logger = LogManager.getLogger(HttpExceptionHandler.class);

  /**
   * Handles given {@link HttpException} and writes an HTML response to the given {@link
   * OutputStream}.
   *
   * <p>Beware that this method will not close the given {@link OutputStream}, the caller needs to
   * take care of that
   *
   * @param exception {@link HttpException} that has been thrown in the {@link HttpHandler}
   * @param out {@link OutputStream} to write the HTML answer to
   */
  public static void handleException(HttpException exception, OutputStream out) {
    logger.error(
        "Error occurred during  HTTP request handling, trying to handle the following exception",
        exception);
    try {

      final HttpStatusCode statusCode = exception.getHttpStatusCode();
      final StringBuilder sb = new StringBuilder();
      final HttpResponse.Builder responseBuilder = new HttpResponse.Builder(out);

      if (statusCode.getCode() >= HttpStatusCode.BAD_REQUEST.getCode()
          && statusCode.getCode() <= HttpStatusCode.HTTP_VERSION_NOT_SUPPORTED.getCode()) {
        sb.append("<html><head>");
        sb.append("<title>")
            .append(statusCode.getCode())
            .append("-")
            .append(statusCode.getPhrase())
            .append("</title></head><body>");
        sb.append("<h2>ERROR #")
            .append(statusCode.getCode())
            .append("-")
            .append(statusCode.getPhrase())
            .append("</h2>");
        sb.append("</body></html>");
        responseBuilder.statusCode(statusCode).messageBody(sb).build().sendResponse();
      } else {
        logger.info(
            "The given HttpErrorCode is not an error: {} - {}",
            statusCode.getCode(),
            statusCode.getPhrase());
        responseBuilder.statusCode(statusCode).build().sendResponse();
      }

    } catch (HttpResponseException hre) {
      logger.fatal("Couldn't handle HttpException!", hre);
    }
  }
}
