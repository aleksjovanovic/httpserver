package de.monoceros.httpserver.server.handler;

import de.monoceros.httpserver.constant.HttpStatusCode;
import de.monoceros.httpserver.exception.HttpException;
import de.monoceros.httpserver.exception.HttpResponseException;
import de.monoceros.httpserver.server.HttpRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.OutputStream;
import java.nio.file.Path;

public class HttpPutRequestHandler extends HttpRequestHandler {

  private static final Logger logger = LogManager.getLogger(HttpPutRequestHandler.class);

  public HttpPutRequestHandler(HttpRequest httpRequest, Path rootDir, OutputStream out) {
    super(httpRequest, rootDir, out);
  }

  @Override
  public void handle() throws HttpException, HttpResponseException {
    // TODO: Read Lock and Cache Invalidation
    throw new HttpException(HttpStatusCode.NOT_IMPLEMENTED);
  }
}
