package de.monoceros.httpserver.server.handler;

import de.monoceros.httpserver.constant.HttpHeader;
import de.monoceros.httpserver.constant.HttpMethod;
import de.monoceros.httpserver.constant.HttpStatusCode;
import de.monoceros.httpserver.exception.HttpException;
import de.monoceros.httpserver.exception.HttpResponseException;
import de.monoceros.httpserver.server.HttpRequest;
import de.monoceros.httpserver.server.HttpResponse;
import de.monoceros.httpserver.util.HttpServerUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;

/**
 * Class which handles requests with If-Modified-Since header described in RFC 7232.
 *
 * @see <a href="https://tools.ietf.org/html/rfc7232#section-3.3">RFC 7232 - If-Modified-Since</a>
 */
public class HttpIfModifiedSinceHandler extends HttpRequestHandler {

  private static final Logger logger = LogManager.getLogger(HttpIfModifiedSinceHandler.class);

  public HttpIfModifiedSinceHandler(HttpRequest httpRequest, Path rootDir, OutputStream out) {
    super(httpRequest, rootDir, out);
  }

  @Override
  public void handle()
      throws HttpException, HttpResponseException {

    final HttpRequestHandler nextHandler = new HttpIfMatchHandler(httpRequest, rootDir, out);

    if (httpRequest.hasHeader(HttpHeader.IF_MODIFIED_SINCE)) {
      final HttpMethod method = httpRequest.getMethod();

      // If-Modified-Since can only be used with GET or HEAD
      if (method.equals(HttpMethod.GET) || method.equals(HttpMethod.HEAD)) {

        // Ignore if used in combination with If-None-Match
        if (httpRequest.hasHeader(HttpHeader.IF_NONE_MATCH)) {
          handleNext(nextHandler);
          return;
        }

        final Path requestedFile = Paths.get(rootDir.toString(), httpRequest.getRequestUri());
        final String ifLastModifiedValue =
            httpRequest.getHeaders().get(HttpHeader.IF_MODIFIED_SINCE.getName());

        try {
          final FileTime sentIfModifiedSinceTime = HttpServerUtil.getFileTimeFromString(ifLastModifiedValue);
          final FileTime requestedFileLastModifiedTime = Files.getLastModifiedTime(requestedFile);

          if (requestedFileLastModifiedTime.compareTo(sentIfModifiedSinceTime) > 0) {
            logger.debug("The requested file has been modified since the given date: Proceed further (200)");
            handleNext(nextHandler);
            return;
          } else {
            logger.debug("The requested file hasn't been modified since the given date: Respond with 304");
            new HttpResponse.Builder(out)
                .statusCode(HttpStatusCode.NOT_MODIFIED)
                .addHeader(
                    HttpHeader.LAST_MODIFIED.getName(),
                    HttpServerUtil.getDateFromMillis(requestedFileLastModifiedTime.toMillis()))
                .build()
                .sendResponse();
            return; // ABORT
          }
        } catch (IOException ioe) {
          logger.error("Error occurred during If-Modified-Since handling!", ioe);
          throw new HttpException(HttpStatusCode.INTERNAL_SERVER_ERROR, ioe);
        }
      }
    }

    handleNext(nextHandler);
  }
}
