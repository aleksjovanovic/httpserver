package de.monoceros.httpserver.server.handler;

import de.monoceros.httpserver.constant.HttpMethod;
import de.monoceros.httpserver.constant.HttpStatusCode;
import de.monoceros.httpserver.exception.HttpException;
import de.monoceros.httpserver.exception.HttpResponseException;
import de.monoceros.httpserver.server.HttpRequest;

import java.io.OutputStream;
import java.nio.file.Path;

/** Class that handles requests based on the HTTP method. */
public class HttpMethodRequestHandler extends HttpRequestHandler {

  public HttpMethodRequestHandler(HttpRequest httpRequest, Path rootDir, OutputStream out) {
    super(httpRequest, rootDir, out);
  }

  @Override
  public void handle() throws HttpException, HttpResponseException {
    // Get Method
    final HttpMethod method = httpRequest.getMethod();

    // Delegate Methods
    if (method.equals(HttpMethod.GET) || method.equals(HttpMethod.HEAD)) {
      new HttpGetHeadRequestHandler(httpRequest, rootDir, out).handle();
    } else if (method.equals(HttpMethod.PUT)) {
      new HttpPutRequestHandler(httpRequest, rootDir, out).handle();
    } else {
      throw new HttpException(HttpStatusCode.NOT_IMPLEMENTED);
    }
  }
}
