package de.monoceros.httpserver.server.handler;

import de.monoceros.httpserver.constant.HttpMethod;
import de.monoceros.httpserver.constant.HttpStatusCode;
import de.monoceros.httpserver.exception.HttpException;
import de.monoceros.httpserver.exception.HttpResponseException;
import de.monoceros.httpserver.server.HttpRequest;
import de.monoceros.httpserver.server.HttpResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/** Class that handles missing resource requests */
public class HttpMissingResourceHandler extends HttpRequestHandler {

  private static final Logger logger = LogManager.getLogger(HttpMissingResourceHandler.class);

  public HttpMissingResourceHandler(HttpRequest httpRequest, Path rootDir, OutputStream out) {
    super(httpRequest, rootDir, out);
  }

  @Override
  public void handle()
      throws HttpResponseException, HttpException {
    final HttpRequestHandler nextHandler = new HttpIfModifiedSinceHandler(httpRequest, rootDir, out);
    final HttpMethod method = httpRequest.getMethod();
    if (method.equals(HttpMethod.GET) || method.equals(HttpMethod.HEAD)) {
      final Path requestedFile = Paths.get(rootDir.toString(), httpRequest.getRequestUri());
      if (!Files.exists(requestedFile)) {
        logger.warn("Requested resource does not exist: {}", httpRequest.getRequestUri());
        new HttpResponse.Builder(out).statusCode(HttpStatusCode.NOT_FOUND).build().sendResponse();
        return; // ABORT
      }
    }

    // Next Chain
    handleNext(nextHandler);
  }
}
