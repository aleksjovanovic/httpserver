package de.monoceros.httpserver.server.handler;

import de.monoceros.httpserver.constant.HttpMethod;
import de.monoceros.httpserver.constant.HttpStatusCode;
import de.monoceros.httpserver.exception.HttpException;
import de.monoceros.httpserver.exception.HttpResponseException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;

import de.monoceros.httpserver.server.HttpRequest;
import de.monoceros.httpserver.server.HttpResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/** Handler for favicon requests. */
public class FaviconRequestHandler extends HttpRequestHandler {

  private static final String FAVICON = "favicon.ico";
  private static final Logger logger = LogManager.getLogger(FaviconRequestHandler.class);

  public FaviconRequestHandler(HttpRequest httpRequest, Path rootDir, OutputStream out) {
    super(httpRequest, rootDir, out);
  }

  @Override
  public void handle() throws HttpException, HttpResponseException {

    final HttpRequestHandler nextHandler = new ThreadPauseRequestHandler(httpRequest, rootDir, out);
    final HttpResponse.Builder responseBuilder = new HttpResponse.Builder(out);

    if (httpRequest.getMethod().equals(HttpMethod.GET)
        && httpRequest.getRequestUri().equals("/" + FAVICON)) {
      logger.debug("Handling favicon request");
      try (InputStream favicon = getClass().getClassLoader().getResource(FAVICON).openStream()) {
        responseBuilder.statusCode(HttpStatusCode.OK).messageBody(favicon).build().sendResponse();
      } catch (IOException e) {
        logger.error("Could not find favicon in resources", e);
      }
      return; // ABORT
    }

    // Next Chain
    handleNext(nextHandler);
  }
}
