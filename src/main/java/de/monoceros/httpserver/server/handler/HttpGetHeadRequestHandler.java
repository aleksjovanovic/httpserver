package de.monoceros.httpserver.server.handler;

import de.monoceros.httpserver.constant.HttpMethod;
import de.monoceros.httpserver.constant.HttpStatusCode;
import de.monoceros.httpserver.exception.HttpException;
import de.monoceros.httpserver.exception.HttpResponseException;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import de.monoceros.httpserver.server.HttpRequest;
import de.monoceros.httpserver.server.HttpResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Handles GET request described in RFC 2616.
 *
 * @see <a href="https://tools.ietf.org/html/rfc2616#section-9.3">RFC 2616 - GET</a>
 * @see <a href="https://tools.ietf.org/html/rfc2616#section-9.4">RFC 2616 - HEAD</a>
 */
public class HttpGetHeadRequestHandler extends HttpRequestHandler {

  private static final Logger logger = LogManager.getLogger(HttpGetHeadRequestHandler.class);

  public HttpGetHeadRequestHandler(HttpRequest httpRequest, Path rootDir, OutputStream out) {
    super(httpRequest, rootDir, out);
  }

  @Override
  public void handle() throws HttpException, HttpResponseException {

    final HttpMethod method = httpRequest.getMethod();
    final String requestUri = httpRequest.getRequestUri();
    final Path requestedFile = Paths.get(rootDir.toString(), requestUri);
    final HttpResponse.Builder responseBuilder = new HttpResponse.Builder(out);

    if (Files.exists(requestedFile)) {
      if (Files.isDirectory(requestedFile)) {
        logger.debug("{} is a directory", requestedFile.toString());
        responseBuilder
            .statusCode(HttpStatusCode.OK)
            .messageBody(directoryList(requestedFile, rootDir));
      } else {
        logger.debug("Requested file {} is a file", requestedFile.toString());
        responseBuilder.statusCode(HttpStatusCode.OK).messageBody(requestedFile);
      }
    } else {
      throw new HttpException(HttpStatusCode.NOT_FOUND);
    }

    // If HEAD then skip message body creation
    if (method.equals(HttpMethod.HEAD)) {
      responseBuilder.skipMessageBody(true);
    }

    responseBuilder.build().sendResponse();

    return; // End of Chain
  }

  /**
   * Creates a HTML Response listing all files of a Directory.
   *
   * @param dir Directory that needs to be listed
   * @param rootDir root Directory
   * @return {@link StringBuilder} containing the HTML structure
   * @throws HttpException if file listing fails
   */
  private StringBuilder directoryList(Path dir, Path rootDir) throws HttpException {
    StringBuilder sb = new StringBuilder();
    sb.append("<html><head><title>This is a Directory</title></head><body>");

    try {
      Files.list(dir)
          .forEach(
              file ->
                  sb.append(
                      String.format(
                          "<a href=\"/%s\">%s</a><br>",
                          file.subpath(rootDir.getNameCount(), file.getNameCount()),
                          file.getFileName())));
    } catch (IOException ioe) {
      logger.error("Creating directory list from {} has failed", dir.toString(), ioe);
      throw new HttpException(HttpStatusCode.INTERNAL_SERVER_ERROR);
    }

    sb.append("</body></html>");
    return sb;
  }
}
