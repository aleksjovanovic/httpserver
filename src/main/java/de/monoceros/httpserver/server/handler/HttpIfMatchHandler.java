package de.monoceros.httpserver.server.handler;

import de.monoceros.httpserver.constant.HttpHeader;
import de.monoceros.httpserver.constant.HttpStatusCode;
import de.monoceros.httpserver.exception.HttpException;
import de.monoceros.httpserver.exception.HttpResponseException;
import de.monoceros.httpserver.server.ETagCache;
import de.monoceros.httpserver.server.HttpRequest;
import de.monoceros.httpserver.server.HttpResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.StringTokenizer;

/**
 * Class which handles If-Match header requests describe in RFC 7232.
 *
 * @see <a href="https://tools.ietf.org/html/rfc7232#section-3.1">RFC 7232 - If-Match</a>
 */
public class HttpIfMatchHandler extends HttpRequestHandler {

  private static final Logger logger = LogManager.getLogger(HttpIfMatchHandler.class);

  public HttpIfMatchHandler(HttpRequest httpRequest, Path rootDir, OutputStream out) {
    super(httpRequest, rootDir, out);
  }

  @Override
  public void handle() throws HttpResponseException, HttpException {
    final HttpRequestHandler nextHandler = new HttpIfNoneMatchHandler(httpRequest, rootDir, out);

    if (httpRequest.hasHeader(HttpHeader.IF_MATCH)) {
      final String headerValue = httpRequest.getHeaders().get(HttpHeader.IF_MATCH.getName());
      final StringTokenizer commaTokenizer = new StringTokenizer(headerValue, ",", false);
      logger.debug("Handling If-Match header: {}", headerValue);

      while (commaTokenizer.hasMoreTokens()) {
        final String eTag = commaTokenizer.nextToken().trim();
        if (eTag.equals("*")) {
          logger.debug("* found: CONTINUE");
          handleNext(nextHandler);
          return;
        }

        final Path requestedFile = Paths.get(rootDir.toString(), httpRequest.getRequestUri());
        final ETagCache cache = ETagCache.getInstance();

        // Only Strong validation
        if (ETagCache.strongCompare(eTag, cache.getETag(requestedFile))) {
          logger.debug("ETags match: CONTINUE");
          handleNext(nextHandler);
          return;
        }
      }

      logger.debug(
          "No If-Match! return respond {} - {}",
          HttpStatusCode.PRECONDITION_FAILED.getCode(),
          HttpStatusCode.PRECONDITION_FAILED.getPhrase());

      final HttpResponse.Builder responseBuilder = new HttpResponse.Builder(out);
      responseBuilder.statusCode(HttpStatusCode.PRECONDITION_FAILED).build().sendResponse();
      return; // ABORT
    }

    // Continue in Chain
    handleNext(nextHandler);
  }
}
