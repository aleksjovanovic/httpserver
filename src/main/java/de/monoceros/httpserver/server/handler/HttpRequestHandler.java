package de.monoceros.httpserver.server.handler;

import de.monoceros.httpserver.exception.HttpException;
import de.monoceros.httpserver.exception.HttpResponseBuilderException;
import de.monoceros.httpserver.exception.HttpResponseException;
import de.monoceros.httpserver.server.HttpRequest;

import java.io.OutputStream;
import java.nio.file.Path;

/** Interface for HTTP request handler. */
public abstract class HttpRequestHandler {

  HttpRequest httpRequest;
  Path rootDir;
  OutputStream out;

  public HttpRequestHandler(HttpRequest httpRequest, Path rootDir, OutputStream out) {
    this.httpRequest = httpRequest;
    this.rootDir = rootDir;
    this.out = out;
  }

  /**
   * Handles the given HTTP Request.
   *
   * <p>This method will not close the OutputStream, the caller needs to take care of that.
   *
   * @return <b>true</b> if the caller should continue and <b>false</b> if not
   * @throws HttpException if something general wen't wrong
   * @throws HttpResponseBuilderException if messageBody type is wrong
   * @throws HttpResponseException if sending response wen't wrong
   */
  public abstract void handle() throws HttpException, HttpResponseException;

  public void handleNext(HttpRequestHandler nextHandler) throws HttpException, HttpResponseException {
    nextHandler.handle();
  }
}
