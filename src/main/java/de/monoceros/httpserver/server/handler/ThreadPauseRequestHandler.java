package de.monoceros.httpserver.server.handler;

import de.monoceros.httpserver.constant.HttpHeader;
import de.monoceros.httpserver.exception.HttpException;
import de.monoceros.httpserver.exception.HttpResponseException;
import de.monoceros.httpserver.server.HttpRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.OutputStream;
import java.nio.file.Path;

/**
 * Implements a thread pause handler which will pause the current thread if the correct header is
 * sent. This class is only for debugging purposes to test concurrency.
 */
public class ThreadPauseRequestHandler extends HttpRequestHandler {

  private static final Logger logger = LogManager.getLogger(ThreadPauseRequestHandler.class);

  public ThreadPauseRequestHandler(HttpRequest httpRequest, Path rootDir, OutputStream out) {
    super(httpRequest, rootDir, out);
  }

  /**
   * Handles Thread Pause if THREAD_PAUSE {@link HttpHeader} is sent.
   *
   * <p>This method will not close the OutputStream, the caller needs to take care of that.
   */
  @Override
  public void handle() throws HttpException, HttpResponseException {
    final HttpRequestHandler nextHandler = new HttpMissingResourceHandler(httpRequest, rootDir, out);

    if (httpRequest.hasHeader(HttpHeader.THREAD_PAUSE)) {
      logger.debug("Handling tread pause request!");
      try {
        long pauseInMillis =
            Long.parseLong(httpRequest.getHeaders().get(HttpHeader.THREAD_PAUSE.getName()));
        logger.debug("Pausing current thread for {} milliseconds", pauseInMillis);
        Thread.currentThread().sleep(pauseInMillis);
      } catch (Exception e) {
        logger.error("Error occurred during ThreadPause", e);
      }
    }

    // Continue Chain
    handleNext(nextHandler);
  }
}
