package de.monoceros.httpserver.server.handler;

import de.monoceros.httpserver.constant.HttpHeader;
import de.monoceros.httpserver.constant.HttpMethod;
import de.monoceros.httpserver.constant.HttpStatusCode;
import de.monoceros.httpserver.exception.HttpException;
import de.monoceros.httpserver.exception.HttpResponseException;
import de.monoceros.httpserver.server.ETagCache;
import de.monoceros.httpserver.server.HttpRequest;
import de.monoceros.httpserver.server.HttpResponse;
import de.monoceros.httpserver.util.HttpServerUtil;

import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.StringTokenizer;

/**
 * Class which handles If-None-Match headers decribed in RFC 7232.
 *
 * @see <a href="https://tools.ietf.org/html/rfc7232#section-3.2">RFC 7232 - If-None-Match</a>
 */
public class HttpIfNoneMatchHandler extends HttpRequestHandler {

  public HttpIfNoneMatchHandler(HttpRequest httpRequest, Path rootDir, OutputStream out) {
    super(httpRequest, rootDir, out);
  }

  @Override
  public void handle() throws HttpException, HttpResponseException {

    final HttpRequestHandler nextHandler = new HttpMethodRequestHandler(httpRequest, rootDir, out);
    final HttpMethod method = httpRequest.getMethod();

    if (httpRequest.hasHeader(HttpHeader.IF_NONE_MATCH)) {
      final ETagCache cache = ETagCache.getInstance();
      final String ifNoneMatchValue =
          httpRequest.getHeaders().get(HttpHeader.IF_NONE_MATCH.getName());
      final Path requestedFile = Paths.get(rootDir.toString(), httpRequest.getRequestUri());
      final String requestedFileETag = cache.getETag(requestedFile);

      if (hasMatchingEtag(ifNoneMatchValue, requestedFileETag)) {
        // If-None-Match condition failed
        if (method.equals(HttpMethod.HEAD) || method.equals(HttpMethod.GET)) {
          new HttpResponse.Builder(out)
              .statusCode(HttpStatusCode.NOT_MODIFIED)
              .addHeader(HttpHeader.DATE.getName(), HttpServerUtil.getCurrentDate())
              .addHeader(HttpHeader.CONTENT_LOCATION.getName(), requestedFile.getFileName().toString())
              .addHeader(HttpHeader.ETAG.getName(), requestedFileETag)
              .build()
              .sendResponse();
          return; // ABORT
        } else if (method.equals(HttpMethod.PUT)) {
          if (hasMatchingEtag(ifNoneMatchValue, requestedFileETag)) {
            new HttpResponse.Builder(out)
                .statusCode(HttpStatusCode.PRECONDITION_FAILED)
                .build()
                .sendResponse();
            return; // ABORT
          }
          throw new HttpException(HttpStatusCode.NOT_IMPLEMENTED);
        }
      }
    }

    // Continue Chain
    handleNext(nextHandler);
  }

  /**
   * Checks if the value from If-None-Match header is equal to the requested files ETag.
   *
   * @param headerValue values from If-None-Match header
   * @param requestedFilesEtag ETag of requestedFile
   * @return true if match, false if not
   */
  private boolean hasMatchingEtag(String headerValue, String requestedFilesEtag) {
    final StringTokenizer commaTokenizer = new StringTokenizer(headerValue, ",", false);
    while (commaTokenizer.hasMoreTokens()) {
      final String eTag = commaTokenizer.nextToken().trim();
      if (eTag.equals("*")) {
        return true;
      }

      if (eTag.equals(requestedFilesEtag)) {
        return true;
      }
    }
    return false;
  }
}
