package de.monoceros.httpserver.server.handler;

import de.monoceros.httpserver.constant.HttpToken;
import de.monoceros.httpserver.exception.HttpException;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.file.Path;

import de.monoceros.httpserver.server.HttpRequest;
import de.monoceros.httpserver.server.HttpRequestParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HttpHandler implements Runnable {

  private static final Logger logger = LogManager.getLogger(HttpHandler.class);

  private Socket connection;
  private Path rootDir;

  public HttpHandler(Socket connection, Path rootDir) {
    this.connection = connection;
    this.rootDir = rootDir;
  }

  @Override
  public void run() {
    logger.info("Handling incoming request");
    try (InputStream in = connection.getInputStream();
        OutputStream out = connection.getOutputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(in))) {
      try {
        // Check if Buffered Reader is ready
        if (!br.ready()) {
          logger.warn("Input is not ready!");
          return;
        }

        // Read entire HttpRequest
        logger.info("Reading Http Request from input");
        StringBuilder sb = new StringBuilder();
        String line;
        do {
          line = br.readLine();
          sb.append(line).append(HttpToken.CARRIAGE_RETURN_LINE_FEED);
        } while (line != null && line.length() > 0); // read until empty line

        // Parse HttpRequest String
        HttpRequest httpRequest = HttpRequestParser.parse(sb.toString());

        // Enter Chain of Responsibility
        // 1 FaviconHandler
        // 2 Thread Pause Handler
        // 3 404 Handler
        // 4 If-Modified-Since Handler
        // 5 If-Match Handler
        // 6 If-None-Match Handler
        // 7 MethodReqeust Handler
        //   |--> GET/HEAD Handler if method is GET or HEAD
        //   |--> More to come
        FaviconRequestHandler faviconRequestHandler = new FaviconRequestHandler(httpRequest, rootDir, out);
        faviconRequestHandler.handle();
      } catch (HttpException he) {
        HttpExceptionHandler.handleException(he, out);
      }
    } catch (Exception e) {
      logger.fatal("Critical error occurred during HTTP request handling", e);
    }
  }
}
