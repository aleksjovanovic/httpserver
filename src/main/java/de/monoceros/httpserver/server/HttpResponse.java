package de.monoceros.httpserver.server;

import de.monoceros.httpserver.constant.HttpHeader;
import de.monoceros.httpserver.constant.HttpStatusCode;
import de.monoceros.httpserver.constant.HttpToken;
import de.monoceros.httpserver.exception.HttpResponseBuilderException;
import de.monoceros.httpserver.exception.HttpResponseException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import de.monoceros.httpserver.util.HttpServerUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class that represents an HTTP response defined in RFC 2616.
 *
 * <p>This class provides its own {@link HttpResponse.Builder} to build a {@link HttpResponse}
 *
 * @see <a href="https://tools.ietf.org/html/rfc2616#section-6">RFC 2616 - Response</a>
 */
public class HttpResponse {

  private static Logger logger = LogManager.getLogger(HttpResponse.class);

  private OutputStream out;
  private HttpStatusCode responseStatusCode;
  private String responseVersion;
  private Object messageBody;
  private Map<String, String> responseHeaders;
  private Boolean skipMessageBody;

  private HttpResponse(Builder builder) {
    this.out = builder.out;
    this.responseStatusCode = builder.responseStatusCode;
    this.responseVersion = HttpToken.SUPPORTED_HTTP_VERSION;
    this.responseHeaders = builder.responseHeaders;
    this.messageBody = builder.messageBody;
    this.skipMessageBody = builder.skipMessageBody;
  }

  public static class Builder {
    private OutputStream out;
    private HttpStatusCode responseStatusCode;
    private Map<String, String> responseHeaders;
    private Object messageBody;
    private Boolean skipMessageBody;

    /**
     * Builder constructor.
     *
     * @param out output stream to write the response to
     */
    public Builder(OutputStream out) {
      this.out = out;
      this.skipMessageBody = false;
    }

    public Builder statusCode(HttpStatusCode code) {
      this.responseStatusCode = code;
      return this;
    }

    /**
     * Adds a message body to the request builder.
     *
     * <p>A message body can be either be null, a {@link String}, a {@link StringBuilder} or {@link
     * Path}
     *
     * @param body MessageBody object to be returned to client
     * @throws HttpResponseBuilderException if message body is not of valid type
     */
    public Builder messageBody(Object body) throws HttpResponseBuilderException {
      if (body != null
          && !(body instanceof String || body instanceof StringBuilder || body instanceof Path || body instanceof InputStream)) {
        throw new HttpResponseBuilderException(
            "Message Body does not support the class " + body.getClass().getName());
      }
      this.messageBody = body;
      return this;
    }

    public Builder headers(Map<String, String> headers) {
      this.responseHeaders = headers;
      return this;
    }

    /**
     * Adds a HTTP response header to the builder.
     *
     * @param key header name
     * @param value header value
     */
    public Builder addHeader(String key, String value) {
      if (this.responseHeaders == null) {
        this.responseHeaders = new HashMap<>();
      }
      this.responseHeaders.put(key, value);
      return this;
    }

    /**
     * Puts a flag if the message body should be skipped or not.
     *
     * @param skip flag
     */
    public Builder skipMessageBody(boolean skip) {
      this.skipMessageBody = skip;
      return this;
    }

    public HttpResponse build() {
      return new HttpResponse(this);
    }
  }

  /**
   * Sends the response on the given {@link OutputStream}.
   *
   * <p>This method will not close the OutputStream, the caller has to take care of that.
   *
   * @throws HttpResponseException if either the OutputStream is null or something else wen't while
   *     writing on the OutputStream
   */
  public void sendResponse() throws HttpResponseException {
    if (out == null) {
      logger.error("The given OutputStream is not set.");
      throw new HttpResponseException("Output Stream not set");
    }

    try {
      StringBuilder sb = new StringBuilder();
      // Create Status Line
      sb.append(
          String.format(
              "%s %d %s",
              HttpToken.SUPPORTED_HTTP_VERSION,
              responseStatusCode.getCode(),
              responseStatusCode.getPhrase()));
      sb.append(HttpToken.CARRIAGE_RETURN_LINE_FEED);

      // Create Response Headers
      if (responseHeaders != null) {
        responseHeaders.forEach(
            (key, value) -> {
              sb.append(String.format("%s: %s", key, value));
              sb.append(HttpToken.CARRIAGE_RETURN_LINE_FEED);
            });
      }

      // Generate Response Headers if messageBody is a file
      if (messageBody != null && messageBody instanceof Path) {
        final Path requestedFile = (Path) messageBody;
        final FileChannel fileChannel = FileChannel.open(requestedFile);
        final ETagCache cache = ETagCache.getInstance();
        final Long lastModifiedInMillis = Files.getLastModifiedTime(requestedFile).toMillis();

        sb.append(String.format("%s: %s", HttpHeader.DATE.getName(), HttpServerUtil.getCurrentDate()));
        sb.append(HttpToken.CARRIAGE_RETURN_LINE_FEED);
        sb.append(String.format("%s: %s", HttpHeader.ETAG.getName(), cache.getETag(requestedFile)));
        sb.append(HttpToken.CARRIAGE_RETURN_LINE_FEED);
        sb.append(String.format("%s: %d", HttpHeader.CONTENT_LENGTH.getName(), fileChannel.size()));
        sb.append(HttpToken.CARRIAGE_RETURN_LINE_FEED);
        sb.append(String.format("%s: %s", HttpHeader.CONTENT_LOCATION.getName(), requestedFile.getFileName().toString()));
        sb.append(HttpToken.CARRIAGE_RETURN_LINE_FEED);
        sb.append(String.format("%s: %s", HttpHeader.LAST_MODIFIED.getName(), HttpServerUtil.getDateFromMillis(lastModifiedInMillis)));
        sb.append(HttpToken.CARRIAGE_RETURN_LINE_FEED);
      }

      // Create Empty Line
      sb.append(HttpToken.CARRIAGE_RETURN_LINE_FEED);

      // Write Http Response
      out.write(sb.toString().getBytes());
      out.flush();

      // Write Message Body
      if (!skipMessageBody && messageBody != null) {
        if (messageBody instanceof String) {
          out.write(((String) messageBody).getBytes());
        } else if (messageBody instanceof StringBuilder) {
          out.write(((StringBuilder) messageBody).toString().getBytes());
        } else if (messageBody instanceof Path) {
          Files.copy((Path) messageBody, out);
        } else if (messageBody instanceof InputStream) {
          ((InputStream)messageBody).transferTo(out);
        }
        out.flush();
      }
    } catch (IOException ioe) {
      logger.error("Critical error occurred during HttpResponse", ioe);
      throw new HttpResponseException("Couldn't send response.", ioe);
    }
  }
}
