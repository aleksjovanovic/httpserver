package de.monoceros.httpserver.server;

import de.monoceros.httpserver.constant.HttpHeader;
import de.monoceros.httpserver.constant.HttpMethod;
import de.monoceros.httpserver.constant.HttpToken;
import de.monoceros.httpserver.exception.HttpParseException;
import de.monoceros.httpserver.exception.InvalidHttpMethodException;
import de.monoceros.httpserver.exception.MandatoryFieldsException;
import de.monoceros.httpserver.exception.UnsupportedHttpVersionException;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class represents a HTTP request defined in RFC 2616.
 *
 * <p>It provides its own {@link HttpRequest.Builder} to build a {@link HttpRequest}
 *
 * @see <a href="https://tools.ietf.org/html/rfc2616#section-5">RFC 2616 - Request</a>
 */
public class HttpRequest {

  private static Logger logger = LogManager.getLogger(HttpRequest.class);

  private HttpMethod method;
  private String requestUri;
  private String version;
  private Map<String, String> headers;

  private HttpRequest(Builder builder) {
    this.method = HttpMethod.valueOf(builder.method);
    this.requestUri = builder.requestUri;
    this.version = builder.version;
    this.headers = builder.headers;
  }

  public HttpMethod getMethod() {
    return method;
  }

  public String getRequestUri() {
    return requestUri;
  }

  public String getVersion() {
    return version;
  }

  public Map<String, String> getHeaders() {
    return headers;
  }

  /**
   * Check if this request has the given {@link HttpHeader}.
   *
   * @param header {@link HttpHeader} to check
   * @return true if request has given {@link HttpHeader}, false if not
   */
  public boolean hasHeader(HttpHeader header) {
    if (this.headers != null) {
      return this.headers.containsKey(header.getName());
    }
    return false;
  }

  /**
   * Builder class that builds a {@link HttpRequest}.
   */
  public static class Builder {
    private String method;
    private String requestUri;
    private String version;
    private Map<String, String> headers;

    public Builder() {
    }

    public Builder method(String method) {
      this.method = method;
      return this;
    }

    public Builder requestUri(String requestUri) {
      this.requestUri = requestUri;
      return this;
    }

    public Builder version(String version) {
      this.version = version;
      return this;
    }

    /**
     * Adds a new Request Header.
     *
     * @param key headers name
     * @param value headers value
     */
    public Builder addHeader(String key, String value) {
      if (this.headers == null) {
        this.headers = new HashMap<>();
      }
      this.headers.put(key, value);
      return this;
    }

    /**
     * Builds a {@link HttpRequest}.
     *
     * @return {@link HttpRequest}
     * @throws HttpParseException if one or more mandatory fields are missing. Mandatory fields are
     *     method, version and requestUri
     */
    public HttpRequest build() throws HttpParseException {
      logger.debug("Check mandatory fields");
      if (method == null || version == null || requestUri == null) {
        throw new MandatoryFieldsException(
            String.format(
                "Mandatory fields are missing! method: %b version: %b requestUri: %b",
                (method != null), (version != null), (requestUri != null)));
      }

      logger.debug("Check Http Version");
      if (!HttpToken.SUPPORTED_HTTP_VERSION.equals(version)) {
        throw new UnsupportedHttpVersionException(
            String.format("The version %s is not supported", version));
      }

      try {
        logger.debug("Check Http Method");
        HttpMethod.valueOf(method);
      } catch (IllegalArgumentException iae) {
        throw new InvalidHttpMethodException(
            String.format("The method %s is not a valid HTTP method", method));
      }

      return new HttpRequest(this);
    }
  }
}
