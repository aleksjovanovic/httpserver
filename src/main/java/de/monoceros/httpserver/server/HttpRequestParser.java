package de.monoceros.httpserver.server;

import de.monoceros.httpserver.constant.HttpToken;
import de.monoceros.httpserver.exception.HttpParseException;

import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class that is responsible to parse Http requests defined in RFC 2616.
 *
 * @see <a href="https://tools.ietf.org/html/rfc2616#section-5">RFC 2616 - Request</a>
 */
public class HttpRequestParser {

  private static Logger logger = LogManager.getLogger(HttpRequestParser.class);

  /**
   * Parse a Http Request from string.
   *
   * @param httpRequest HttpRequest as string
   * @return Populated {@link HttpRequest} from given string
   * @throws HttpParseException throws HttpParseException if request string can't be parsed
   */
  public static HttpRequest parse(String httpRequest) throws HttpParseException {
    if (httpRequest == null || httpRequest.length() == 0) {
      throw new HttpParseException("Http Request is empty");
    }

    HttpRequest.Builder httpRequestBuilder = new HttpRequest.Builder();

    try {
      logger.debug("Parsing httpRequest: {}", httpRequest);
      StringTokenizer newlineTokenizer =
          new StringTokenizer(httpRequest, HttpToken.CARRIAGE_RETURN_LINE_FEED, false);

      logger.debug("Reading Request Line");
      String requestLine = newlineTokenizer.nextToken();
      StringTokenizer spaceTokenizer = new StringTokenizer(requestLine, HttpToken.SPACE, false);

      httpRequestBuilder
          .method(spaceTokenizer.nextToken())
          .requestUri(spaceTokenizer.nextToken())
          .version(spaceTokenizer.nextToken());

      logger.debug("Reading headers");
      while (newlineTokenizer.hasMoreTokens()) {
        String header = newlineTokenizer.nextToken();
        StringTokenizer headerTokenizer = new StringTokenizer(header, ":", false);
        String key = headerTokenizer.nextToken().trim();
        String value = headerTokenizer.nextToken().trim();

        // In case the header value contains a ":" delimiter we have to add it to the value
        while (headerTokenizer.hasMoreTokens()) {
          value += ":" + headerTokenizer.nextToken();
        }

        httpRequestBuilder.addHeader(key, value);
      }
    } catch (NoSuchElementException nsee) {
      // tokenizer.next() failed
      throw new HttpParseException("Http Request structure is invalid");
    }

    return httpRequestBuilder.build();
  }
}
