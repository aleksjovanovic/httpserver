package de.monoceros.httpserver.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Path;

import de.monoceros.httpserver.server.handler.HttpHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/** Http Server class that listens to incoming requests. */
public class HttpServer implements Runnable {

  private static Logger logger = LogManager.getLogger(HttpServer.class);

  private int port = 8080;
  private Path rootDir = null;
  private boolean isRunning = false;
  private ServerSocket serverSocket = null;

  public HttpServer(int port, Path rootDir) {
    this.port = port;
    this.rootDir = rootDir;
  }

  /** Run Method that starts the HttpServer. */
  @Override
  public void run() {
    try {
      logger.info("Starting server on port: {} with root directory: {}", port, rootDir);
      serverSocket = new ServerSocket(port);
      this.isRunning = true;

      while (isRunning()) {
        try {
          Socket socket = this.serverSocket.accept();
          if (socket != null) {
            logger.info("Incoming request, start new handler");
            new Thread(new HttpHandler(socket, rootDir)).start();
          }
        } catch (IOException ioe) {
          logger.error("Unknown error has occurred", ioe);
        }
      }
    } catch (IOException ioe) {
      logger.error("Starting server has failed", ioe);
    }
  }

  public synchronized boolean isRunning() {
    return this.isRunning;
  }

  public synchronized void stopServer() {
    logger.info("Stopping server");
    this.isRunning = false;
  }
}
