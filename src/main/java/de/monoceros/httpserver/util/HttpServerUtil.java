package de.monoceros.httpserver.util;

import java.nio.file.attribute.FileTime;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

/** General purpose Util class */
public class HttpServerUtil {

  /**
   * Gets the current date in a date format specified in RFC 1123.
   *
   * @see <a href="https://tools.ietf.org/html/rfc1123">RFC 1123</a>
   * @return current RFC 1123 date as String
   */
  public static String getCurrentDate() {
    return DateTimeFormatter.RFC_1123_DATE_TIME.format(ZonedDateTime.now(ZoneOffset.UTC));
  }

  /**
   * Gets a RFC 1123 date as string from given milliseconds.
   *
   * @see <a href="https://tools.ietf.org/html/rfc1123">RFC 1123</a>
   * @param millis given milliseconds (epoch)
   * @return properly formatted RFC 1123 date string
   */
  public static String getDateFromMillis(long millis) {
    return DateTimeFormatter.RFC_1123_DATE_TIME
        .withZone(ZoneOffset.UTC)
        .format(FileTime.from(millis, TimeUnit.MILLISECONDS).toInstant());
  }

  /**
   * Gets a FileTime from RFC 1123 formatted date string.
   *
   * @return corresponding FileTime
   */
  public static FileTime getFileTimeFromString(String dateString) {
    Instant instant = Instant.from(DateTimeFormatter.RFC_1123_DATE_TIME.withZone(ZoneOffset.UTC).parse(dateString));
    return FileTime.from(instant);
  }
}
