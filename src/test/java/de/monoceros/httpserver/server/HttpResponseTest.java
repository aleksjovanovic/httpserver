package de.monoceros.httpserver.server;

import de.monoceros.httpserver.constant.HttpHeader;
import de.monoceros.httpserver.constant.HttpStatusCode;
import de.monoceros.httpserver.constant.HttpToken;
import de.monoceros.httpserver.exception.HttpException;
import de.monoceros.httpserver.exception.HttpResponseBuilderException;
import de.monoceros.httpserver.exception.HttpResponseException;
import de.monoceros.httpserver.util.HttpServerUtil;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.StringTokenizer;

import static org.junit.Assert.*;

public class HttpResponseTest {

  private Path targetFile;
  private Path rootDir;

  @Before
  public void init() {
    targetFile = Paths.get(getClass().getClassLoader().getResource("awesome.html").getFile());
    rootDir = targetFile.getParent();
  }

  @Test
  public void shouldReturnCorrectResponse() throws Exception {
    // given
    final ByteArrayOutputStream out = new ByteArrayOutputStream();
    final HttpResponse.Builder responseBuilder = new HttpResponse.Builder(out);
    final String contentLocationValue = "/test/this/is/it";
    final String messageBody = "<html><head></head><body><h2>test</h2></body></html>";

    // when
    responseBuilder
        .statusCode(HttpStatusCode.NOT_MODIFIED)
        .addHeader(HttpHeader.CONTENT_LOCATION.getName(), contentLocationValue)
        .messageBody(messageBody)
        .build()
        .sendResponse();

    // then
    System.out.println("\n" + out.toString());
    assertEquals(
        out.toString(),
        "HTTP/1.1 304 Not Modified"
            + HttpToken.CARRIAGE_RETURN_LINE_FEED
            + "Content-Location: /test/this/is/it"
            + HttpToken.CARRIAGE_RETURN_LINE_FEED
            + HttpToken.CARRIAGE_RETURN_LINE_FEED
            + "<html><head></head><body><h2>test</h2></body></html>");
  }

  @Test(expected = HttpResponseException.class)
  public void shouldThrowHttpExceptionIfOutputStreamIsNull() throws HttpResponseException {
    // given
    final HttpResponse.Builder responseBuilder = new HttpResponse.Builder(null);

    // when & then
    responseBuilder.skipMessageBody(true).statusCode(HttpStatusCode.OK).build().sendResponse();
  }

  @Test(expected = HttpResponseException.class)
  public void shouldThrowHttpExceptionIfMessageBodyTypeIsNotSupported()
      throws HttpResponseException {
    // given
    final HttpResponse.Builder responseBuilder = new HttpResponse.Builder(null);

    // when & then
    responseBuilder.messageBody(new Date());
  }

  @Test
  public void shouldGenerateProperTagsAndResponseBodyIfResponseIsValidFile()
      throws HttpResponseException {
    // given
    final ByteArrayOutputStream out = new ByteArrayOutputStream();
    final HttpResponse.Builder responseBuilder = new HttpResponse.Builder(out);

    // when
    responseBuilder.messageBody(targetFile).statusCode(HttpStatusCode.OK).build().sendResponse();

    // then
    final StringTokenizer tokenizer = new StringTokenizer(out.toString(), HttpToken.CARRIAGE_RETURN_LINE_FEED, false);
    assertEquals(tokenizer.nextToken(), "HTTP/1.1 200 OK");
    tokenizer.nextToken(); // Skip Date since it is dynamic
    assertEquals(tokenizer.nextToken(), "ETag: \"a81d7e9f\"");
    assertEquals(tokenizer.nextToken(), "Content-Length: 768");
    assertEquals(tokenizer.nextToken(), "Content-Location: awesome.html");
    tokenizer.nextToken(); // Skip Last-Modified since it is dynamic
    assertEquals(tokenizer.nextToken(), "<html>");
    assertEquals(tokenizer.nextToken(), "    <head>");
    assertEquals(tokenizer.nextToken(), "        <title>!Very nice title :)</title>");
    assertEquals(tokenizer.nextToken(), "    </head>");
    assertEquals(tokenizer.nextToken(), "    <body>");
    assertEquals(tokenizer.nextToken(), "        <h2>Here is a caption</h2>");
    assertEquals(tokenizer.nextToken(), "        <p>");
    assertEquals(tokenizer.nextToken(), "            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.");
    assertEquals(tokenizer.nextToken(), "        </p>");
    assertEquals(tokenizer.nextToken(), "    </body>");
    assertEquals(tokenizer.nextToken(), "</html>");
  }

  @Test
  public void shouldSkipMessageBodyIfFlagIsSet()
      throws HttpResponseException {
    // given
    final ByteArrayOutputStream out = new ByteArrayOutputStream();
    final HttpResponse.Builder responseBuilder = new HttpResponse.Builder(out);

    // when
    responseBuilder.messageBody(targetFile).statusCode(HttpStatusCode.OK).skipMessageBody(true).build().sendResponse();

    // then
    final StringTokenizer tokenizer = new StringTokenizer(out.toString(), HttpToken.CARRIAGE_RETURN_LINE_FEED, false);
    assertEquals(tokenizer.nextToken(), "HTTP/1.1 200 OK");
    tokenizer.nextToken(); // Skip Date since it is dynamic
    assertEquals(tokenizer.nextToken(), "ETag: \"a81d7e9f\"");
    assertEquals(tokenizer.nextToken(), "Content-Length: 768");
    assertEquals(tokenizer.nextToken(), "Content-Location: awesome.html");
    tokenizer.nextToken(); // Skip Last-Modified since it is dynamic
    assertFalse(tokenizer.hasMoreElements()); // Ensure that nothing else has been sent
  }
}
