package de.monoceros.httpserver.server;

import org.junit.Test;

import static org.junit.Assert.*;

public class ETagCacheTest {

  @Test
  public void shouldProperlyCompareStrongAndWeakETags() {
    // given
    final String strong1 = "\"1\"";
    final String weak1 = "W/\"1\"";
    final String weak2 = "W/\"2\"";

    // when
    boolean strongCompareWeak1WithWeak1 = ETagCache.strongCompare(weak1, weak1);
    boolean strongCompareWeak1WithWeak2 = ETagCache.strongCompare(weak1, weak2);
    boolean strongCompareWeak1WithStrong1 = ETagCache.strongCompare(weak1, strong1);
    boolean strongCompareStrong1WithStrong1 = ETagCache.strongCompare(strong1, strong1);

    boolean weakCompareWeak1WithWeak1 = ETagCache.weakCompare(weak1, weak1);
    boolean weakCompareWeak1WithWeak2 = ETagCache.weakCompare(weak1, weak2);
    boolean weakCompareWeak1WithStrong1 = ETagCache.weakCompare(weak1, strong1);
    boolean weakCompareStrong1WithStrong1 = ETagCache.strongCompare(strong1, strong1);

    // then
    assertFalse(strongCompareWeak1WithWeak1);
    assertFalse(strongCompareWeak1WithWeak2);
    assertFalse(strongCompareWeak1WithStrong1);
    assertTrue(strongCompareStrong1WithStrong1);

    assertTrue(weakCompareWeak1WithWeak1);
    assertFalse(weakCompareWeak1WithWeak2);
    assertTrue(weakCompareWeak1WithStrong1);
    assertTrue(weakCompareStrong1WithStrong1);

  }
}