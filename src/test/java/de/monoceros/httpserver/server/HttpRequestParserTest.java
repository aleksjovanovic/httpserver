package de.monoceros.httpserver.server;

import de.monoceros.httpserver.constant.HttpMethod;
import de.monoceros.httpserver.constant.HttpToken;
import de.monoceros.httpserver.exception.HttpParseException;
import de.monoceros.httpserver.exception.InvalidHttpMethodException;
import de.monoceros.httpserver.exception.UnsupportedHttpVersionException;
import org.junit.Test;

import static org.junit.Assert.*;

public class HttpRequestParserTest {

  @Test
  public void shouldParseValidHttpRequest() throws HttpParseException {
    // given
    StringBuilder sb = new StringBuilder();
    sb.append("GET /test HTTP/1.1").append(HttpToken.CARRIAGE_RETURN_LINE_FEED);
    sb.append(HttpToken.CARRIAGE_RETURN_LINE_FEED);

    // when
    HttpRequest httpRequest = HttpRequestParser.parse(sb.toString());

    // then
    assertEquals(httpRequest.getMethod(), HttpMethod.GET);
    assertEquals(httpRequest.getVersion(), HttpToken.SUPPORTED_HTTP_VERSION);
    assertEquals(httpRequest.getRequestUri(), "/test");
  }

  @Test(expected = InvalidHttpMethodException.class)
  public void shouldThrowExceptionWhenInvalidMethod() throws Exception {
    // given
    StringBuilder sb = new StringBuilder();
    sb.append("INVALID /test HTTP/1.1").append(HttpToken.CARRIAGE_RETURN_LINE_FEED);
    sb.append(HttpToken.CARRIAGE_RETURN_LINE_FEED);

    // when & then
    HttpRequestParser.parse(sb.toString());
  }

  @Test(expected = UnsupportedHttpVersionException.class)
  public void shouldThrowExceptionWhenInvalidHttpVersion() throws Exception {
    // given
    StringBuilder sb = new StringBuilder();
    sb.append("GET /test HTTP/1.2").append(HttpToken.CARRIAGE_RETURN_LINE_FEED);
    sb.append(HttpToken.CARRIAGE_RETURN_LINE_FEED);

    // when & then
    HttpRequestParser.parse(sb.toString());
  }

  @Test
  public void shouldParseValidHttpHeader() throws HttpParseException {
    // given
    StringBuilder sb = new StringBuilder();
    sb.append("GET /test HTTP/1.1").append(HttpToken.CARRIAGE_RETURN_LINE_FEED);
    sb.append("header-one: value").append(HttpToken.CARRIAGE_RETURN_LINE_FEED);
    sb.append("header-two: some_other_value").append(HttpToken.CARRIAGE_RETURN_LINE_FEED);
    sb.append(HttpToken.CARRIAGE_RETURN_LINE_FEED);

    // when
    HttpRequest httpRequest = HttpRequestParser.parse(sb.toString());

    // then
    assertEquals(httpRequest.getHeaders().size(), 2);
    assertEquals(httpRequest.getHeaders().get("header-one"), "value");
    assertEquals(httpRequest.getHeaders().get("header-two"), "some_other_value");
  }
}
