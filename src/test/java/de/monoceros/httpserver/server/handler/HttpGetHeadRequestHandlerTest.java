package de.monoceros.httpserver.server.handler;

import de.monoceros.httpserver.constant.HttpHeader;
import de.monoceros.httpserver.constant.HttpMethod;
import de.monoceros.httpserver.constant.HttpToken;
import de.monoceros.httpserver.server.HttpRequest;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.StringTokenizer;

import static org.junit.Assert.*;

public class HttpGetHeadRequestHandlerTest {

  private Path targetFile;
  private Path rootDir;

  @Before
  public void init() {
    targetFile = Paths.get(getClass().getClassLoader().getResource("awesome.html").getFile());
    rootDir = targetFile.getParent();
  }

  @Test
  public void shouldReturn200AndMessageBodyOnValidGetRequest() throws Exception {
    // given
    final ByteArrayOutputStream out = new ByteArrayOutputStream();
    final HttpRequest httpRequest =
        new HttpRequest.Builder()
            .method(HttpMethod.GET.name())
            .version(HttpToken.SUPPORTED_HTTP_VERSION)
            .requestUri("/awesome.html")
            .build();
    final HttpRequestHandler getHeadHandler = new HttpGetHeadRequestHandler(httpRequest, rootDir, out);

    // when
    getHeadHandler.handle();

    // then
    StringTokenizer tokenizer = new StringTokenizer(out.toString(), HttpToken.CARRIAGE_RETURN_LINE_FEED, false);
    assertEquals(tokenizer.nextToken(), "HTTP/1.1 200 OK");
    tokenizer.nextToken(); // Skip Date since it is dynamic
    assertEquals(tokenizer.nextToken(), "ETag: \"a81d7e9f\"");
    assertEquals(tokenizer.nextToken(), "Content-Length: 768");
    assertEquals(tokenizer.nextToken(), "Content-Location: awesome.html");
    tokenizer.nextToken(); // Skip Last-Modified since it is dynamic
    assertEquals(tokenizer.nextToken(), "<html>");
    assertEquals(tokenizer.nextToken(), "    <head>");
    assertEquals(tokenizer.nextToken(), "        <title>!Very nice title :)</title>");
    assertEquals(tokenizer.nextToken(), "    </head>");
    assertEquals(tokenizer.nextToken(), "    <body>");
    assertEquals(tokenizer.nextToken(), "        <h2>Here is a caption</h2>");
    assertEquals(tokenizer.nextToken(), "        <p>");
    assertEquals(tokenizer.nextToken(), "            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.");
    assertEquals(tokenizer.nextToken(), "        </p>");
    assertEquals(tokenizer.nextToken(), "    </body>");
    assertEquals(tokenizer.nextToken(), "</html>");
  }

  @Test
  public void shouldReturn200WithoutMessageBodyOnValidHeadRequest() throws Exception {
    // given
    final ByteArrayOutputStream out = new ByteArrayOutputStream();
    final HttpRequest httpRequest =
        new HttpRequest.Builder()
            .method(HttpMethod.HEAD.name())
            .version(HttpToken.SUPPORTED_HTTP_VERSION)
            .requestUri("/awesome.html")
            .build();
    final HttpRequestHandler getHeadHandler = new HttpGetHeadRequestHandler(httpRequest, rootDir, out);

    // when
    getHeadHandler.handle();

    // then
    StringTokenizer tokenizer = new StringTokenizer(out.toString(), HttpToken.CARRIAGE_RETURN_LINE_FEED, false);
    assertEquals(tokenizer.nextToken(), "HTTP/1.1 200 OK");
    tokenizer.nextToken(); // Skip Date since it is dynamic
    assertEquals(tokenizer.nextToken(), "ETag: \"a81d7e9f\"");
    assertEquals(tokenizer.nextToken(), "Content-Length: 768");
    assertEquals(tokenizer.nextToken(), "Content-Location: awesome.html");
    tokenizer.nextToken(); // Skip Last-Modified since it is dynamic
    assertFalse(tokenizer.hasMoreTokens());
  }
}