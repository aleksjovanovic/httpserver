package de.monoceros.httpserver.server.handler;

import de.monoceros.httpserver.constant.HttpHeader;
import de.monoceros.httpserver.constant.HttpMethod;
import de.monoceros.httpserver.constant.HttpToken;
import de.monoceros.httpserver.server.HttpRequest;
import de.monoceros.httpserver.util.HttpServerUtil;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.util.StringTokenizer;

import static org.junit.Assert.*;

public class HttpIfModifiedSinceHandlerTest {
  private Path targetFile;
  private Path rootDir;

  @Before
  public void init() {
    targetFile = Paths.get(getClass().getClassLoader().getResource("awesome.html").getFile());
    rootDir = targetFile.getParent();
  }

  @Test
  public void shouldRespondWith304IfRequestedFileHasNotBeenModifiedSince() throws Exception {
    // given
    Path fileToCreate = Paths.get(rootDir.toString(), "newFile.html");
    if (Files.exists(fileToCreate)) {
      Files.delete(fileToCreate);
    }
    Files.createFile(fileToCreate);

    FileTime lastModifiedTime = Files.getLastModifiedTime(fileToCreate);
    String lastModifiedInFuture =
        HttpServerUtil.getDateFromMillis(lastModifiedTime.toMillis() + (1000 * 60 * 60 * 1));

    final ByteArrayOutputStream out = new ByteArrayOutputStream();
    final HttpRequest httpRequest =
        new HttpRequest.Builder()
            .method(HttpMethod.GET.name())
            .version(HttpToken.SUPPORTED_HTTP_VERSION)
            .requestUri("/newFile.html")
            .addHeader(HttpHeader.IF_MODIFIED_SINCE.getName(), lastModifiedInFuture)
            .build();
    final HttpRequestHandler ifModifiedSinceHandler =
        new HttpIfModifiedSinceHandler(httpRequest, rootDir, out);

    // when
    ifModifiedSinceHandler.handle();

    // then
    StringTokenizer stringTokenizer = new StringTokenizer(out.toString(), HttpToken.CARRIAGE_RETURN_LINE_FEED, false);
    assertEquals(stringTokenizer.nextToken(), "HTTP/1.1 304 Not Modified");
  }

  @Test
  public void shouldRespondWith200IfRequestedFileHasBeenModifiedSince() throws Exception {
    // given
    Path fileToCreate = Paths.get(rootDir.toString(), "newFile.html");
    if (Files.exists(fileToCreate)) {
      Files.delete(fileToCreate);
    }
    Files.createFile(fileToCreate);

    FileTime lastModifiedTime = Files.getLastModifiedTime(fileToCreate);
    String lstModifiedInPast =
        HttpServerUtil.getDateFromMillis(lastModifiedTime.toMillis() - (1000 * 60 * 60 * 1));

    final ByteArrayOutputStream out = new ByteArrayOutputStream();
    final HttpRequest httpRequest =
        new HttpRequest.Builder()
            .method(HttpMethod.GET.name())
            .version(HttpToken.SUPPORTED_HTTP_VERSION)
            .requestUri("/newFile.html")
            .addHeader(HttpHeader.IF_MODIFIED_SINCE.getName(), lstModifiedInPast)
            .build();
    final HttpRequestHandler ifModifiedSinceHandler =
        new HttpIfModifiedSinceHandler(httpRequest, rootDir, out);

    // when
    ifModifiedSinceHandler.handle();

    // then
    StringTokenizer stringTokenizer = new StringTokenizer(out.toString(), HttpToken.CARRIAGE_RETURN_LINE_FEED, false);
    assertEquals(stringTokenizer.nextToken(), "HTTP/1.1 200 OK");
  }
}
