package de.monoceros.httpserver.server.handler;

import de.monoceros.httpserver.constant.HttpHeader;
import de.monoceros.httpserver.constant.HttpMethod;
import de.monoceros.httpserver.constant.HttpToken;
import de.monoceros.httpserver.exception.HttpParseException;
import de.monoceros.httpserver.server.HttpRequest;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.StringTokenizer;

import static org.junit.Assert.*;

public class FaviconRequestHandlerTest {

  private Path targetFile;
  private Path rootDir;

  @Before
  public void init() {
    targetFile = Paths.get(getClass().getClassLoader().getResource("awesome.html").getFile());
    rootDir = targetFile.getParent();
  }

  @Test
  public void shouldReturnFaviconIfRequested() throws Exception {
    // given
    final ByteArrayOutputStream out = new ByteArrayOutputStream();
    final HttpRequest httpRequest =
        new HttpRequest.Builder()
            .method(HttpMethod.GET.name())
            .version(HttpToken.SUPPORTED_HTTP_VERSION)
            .requestUri("/favicon.ico")
            .build();
    final HttpRequestHandler faviconHandler = new FaviconRequestHandler(httpRequest, rootDir, out);

    // when
    faviconHandler.handle();

    // then
    StringTokenizer tokenizer = new StringTokenizer(out.toString(), HttpToken.CARRIAGE_RETURN_LINE_FEED, false);
    assertEquals(tokenizer.nextToken(), "HTTP/1.1 200 OK");
  }

}