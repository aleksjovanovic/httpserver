package de.monoceros.httpserver.server.handler;

import de.monoceros.httpserver.constant.HttpHeader;
import de.monoceros.httpserver.constant.HttpMethod;
import de.monoceros.httpserver.constant.HttpToken;
import de.monoceros.httpserver.server.ETagCache;
import de.monoceros.httpserver.server.HttpRequest;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.StringTokenizer;

import static org.junit.Assert.*;

public class HttpIfNoneMatchHandlerTest {

  private Path targetFile;
  private Path rootDir;

  @Before
  public void init() {
    targetFile = Paths.get(getClass().getClassLoader().getResource("awesome.html").getFile());
    rootDir = targetFile.getParent();
  }

  @Test
  public void shouldReturn200IfGivenEtagDoesNotMatch() throws Exception {
    // given
    final ByteArrayOutputStream out = new ByteArrayOutputStream();
    final String nonMatchingTag = "non_matching";
    final HttpRequest httpRequest =
        new HttpRequest.Builder()
            .method(HttpMethod.GET.name())
            .version(HttpToken.SUPPORTED_HTTP_VERSION)
            .requestUri("/awesome.html")
            .addHeader(HttpHeader.IF_NONE_MATCH.getName(), nonMatchingTag)
            .build();
    final HttpRequestHandler handler = new HttpIfNoneMatchHandler(httpRequest, rootDir, out);

    // when
    handler.handle();

    // then
    StringTokenizer tokenizer = new StringTokenizer(out.toString(), HttpToken.CARRIAGE_RETURN_LINE_FEED, false);
    assertEquals(tokenizer.nextToken(), "HTTP/1.1 200 OK");
  }

  @Test
  public void shouldReturn304IfGivenTagMatches() throws Exception {
    // given
    final ByteArrayOutputStream out = new ByteArrayOutputStream();
    final String matchingTag = ETagCache.getInstance().getETag(targetFile);
    final HttpRequest httpRequest =
        new HttpRequest.Builder()
            .method(HttpMethod.GET.name())
            .version(HttpToken.SUPPORTED_HTTP_VERSION)
            .requestUri("/awesome.html")
            .addHeader(HttpHeader.IF_NONE_MATCH.getName(), matchingTag)
            .build();
    final HttpRequestHandler handler = new HttpIfNoneMatchHandler(httpRequest, rootDir, out);

    // when
    handler.handle();

    // then
    StringTokenizer tokenizer = new StringTokenizer(out.toString(), HttpToken.CARRIAGE_RETURN_LINE_FEED, false);
    assertEquals(tokenizer.nextToken(), "HTTP/1.1 304 Not Modified");
  }

  @Test
  public void shouldReturn412ForServerSideChangesThatMatchesThatMatchesGivenETag() throws Exception {
    // given
    final ByteArrayOutputStream out = new ByteArrayOutputStream();
    final String matchingTag = ETagCache.getInstance().getETag(targetFile);
    final HttpRequest httpRequest =
        new HttpRequest.Builder()
            .method(HttpMethod.PUT.name())
            .version(HttpToken.SUPPORTED_HTTP_VERSION)
            .requestUri("/awesome.html")
            .addHeader(HttpHeader.IF_NONE_MATCH.getName(), matchingTag)
            .build();
    final HttpRequestHandler handler = new HttpIfNoneMatchHandler(httpRequest, rootDir, out);

    // when
    handler.handle();

    // then
    StringTokenizer tokenizer = new StringTokenizer(out.toString(), HttpToken.CARRIAGE_RETURN_LINE_FEED, false);
    assertEquals(tokenizer.nextToken(), "HTTP/1.1 412 Precondition Failed");
  }

  @Test
  public void shouldReturn304ForWildcardMatcher() throws Exception {
    // given
    final ByteArrayOutputStream out = new ByteArrayOutputStream();
    final String wildcard = "*";
    final HttpRequest httpRequest =
        new HttpRequest.Builder()
            .method(HttpMethod.GET.name())
            .version(HttpToken.SUPPORTED_HTTP_VERSION)
            .requestUri("/awesome.html")
            .addHeader(HttpHeader.IF_NONE_MATCH.getName(), wildcard)
            .build();
    final HttpRequestHandler handler = new HttpIfNoneMatchHandler(httpRequest, rootDir, out);

    // when
    handler.handle();

    // then
    StringTokenizer tokenizer = new StringTokenizer(out.toString(), HttpToken.CARRIAGE_RETURN_LINE_FEED, false);
    assertEquals(tokenizer.nextToken(), "HTTP/1.1 304 Not Modified");
  }

  @Test
  public void shouldReturn304IfOneETagInGivenListMatches() throws Exception {
    // given
    final ByteArrayOutputStream out = new ByteArrayOutputStream();
    final String matchingETag = ETagCache.getInstance().getETag(targetFile);
    final String oneMatchingETagInList = "\"nomatch\", " + matchingETag;
    final HttpRequest httpRequest =
        new HttpRequest.Builder()
            .method(HttpMethod.GET.name())
            .version(HttpToken.SUPPORTED_HTTP_VERSION)
            .requestUri("/awesome.html")
            .addHeader(HttpHeader.IF_NONE_MATCH.getName(), oneMatchingETagInList)
            .build();
    final HttpRequestHandler handler = new HttpIfNoneMatchHandler(httpRequest, rootDir, out);

    // when
    handler.handle();

    // then
    StringTokenizer tokenizer = new StringTokenizer(out.toString(), HttpToken.CARRIAGE_RETURN_LINE_FEED, false);
    assertEquals(tokenizer.nextToken(), "HTTP/1.1 304 Not Modified");
  }

  @Test
  public void shouldReturn200IfNoneGivenETagsInListMatches() throws Exception {
    // given
    final ByteArrayOutputStream out = new ByteArrayOutputStream();
    final String noMatchingETagInList = "\"nomatch\", \"also_not\", \"again_not\"";
    final HttpRequest httpRequest =
        new HttpRequest.Builder()
            .method(HttpMethod.GET.name())
            .version(HttpToken.SUPPORTED_HTTP_VERSION)
            .requestUri("/awesome.html")
            .addHeader(HttpHeader.IF_NONE_MATCH.getName(), noMatchingETagInList)
            .build();
    final HttpRequestHandler handler = new HttpIfNoneMatchHandler(httpRequest, rootDir, out);

    // when
    handler.handle();

    // then
    StringTokenizer tokenizer = new StringTokenizer(out.toString(), HttpToken.CARRIAGE_RETURN_LINE_FEED, false);
    assertEquals(tokenizer.nextToken(), "HTTP/1.1 200 OK");
  }

}