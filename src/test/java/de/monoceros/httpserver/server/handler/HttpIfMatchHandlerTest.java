package de.monoceros.httpserver.server.handler;

import de.monoceros.httpserver.constant.HttpHeader;
import de.monoceros.httpserver.constant.HttpMethod;
import de.monoceros.httpserver.constant.HttpToken;
import de.monoceros.httpserver.server.ETagCache;
import de.monoceros.httpserver.server.HttpRequest;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.StringTokenizer;

import static org.junit.Assert.*;

public class HttpIfMatchHandlerTest {

  private Path targetFile;
  private Path rootDir;

  @Before
  public void init() {
    targetFile = Paths.get(getClass().getClassLoader().getResource("awesome.html").getFile());
    rootDir = targetFile.getParent();
  }

  @Test
  public void shouldRespondWith412IfETagsNotMatch() throws Exception {
    // given
    final ByteArrayOutputStream out = new ByteArrayOutputStream();
    final String noneMatchingETag = "this_cant_match";
    final HttpRequest httpRequest =
        new HttpRequest.Builder()
            .method(HttpMethod.GET.name())
            .version(HttpToken.SUPPORTED_HTTP_VERSION)
            .requestUri("/awesome.html")
            .addHeader(HttpHeader.IF_MATCH.getName(), noneMatchingETag)
            .build();
    final HttpIfMatchHandler ifMatchHandler = new HttpIfMatchHandler(httpRequest, rootDir, out);

    // when
    ifMatchHandler.handle();

    // then
    assertEquals(out.toString(), "HTTP/1.1 412 Precondition Failed\r\n\r\n");
  }

  @Test
  public void shouldContinueIfETagMatches() throws Exception {
    // given
    final ByteArrayOutputStream out = new ByteArrayOutputStream();
    final String matchingEtag = ETagCache.getInstance().getETag(targetFile);
    final HttpRequest httpRequest =
        new HttpRequest.Builder()
            .method(HttpMethod.GET.name())
            .version(HttpToken.SUPPORTED_HTTP_VERSION)
            .requestUri("/awesome.html")
            .addHeader(HttpHeader.IF_MATCH.getName(), matchingEtag)
            .build();
    final HttpIfMatchHandler ifMatchHandler = new HttpIfMatchHandler(httpRequest, rootDir, out);

    // when
    ifMatchHandler.handle();

    // then
    StringTokenizer tokenizer = new StringTokenizer(out.toString(), HttpToken.CARRIAGE_RETURN_LINE_FEED, false);
    assertEquals(tokenizer.nextToken(), "HTTP/1.1 200 OK");
   }

  @Test
  public void shouldRespondWith412IfNoneOfTheETagsMatch() throws Exception {
    // given
    final ByteArrayOutputStream out = new ByteArrayOutputStream();
    final String nonMatchingEtags = "\"cant_match\", \"wont_match\", \"no_match\", \"never_match\"";
    final HttpRequest httpRequest =
        new HttpRequest.Builder()
            .method(HttpMethod.GET.name())
            .version(HttpToken.SUPPORTED_HTTP_VERSION)
            .requestUri("/awesome.html")
            .addHeader(HttpHeader.IF_MATCH.getName(), nonMatchingEtags)
            .build();
    final HttpIfMatchHandler ifMatchHandler = new HttpIfMatchHandler(httpRequest, rootDir, out);

    // when
    ifMatchHandler.handle();

    // then
    assertEquals(out.toString(), "HTTP/1.1 412 Precondition Failed\r\n\r\n");
  }

  @Test
  public void shouldContinueIfOneOfTheETagsMatches() throws Exception {
    // given
    final ByteArrayOutputStream out = new ByteArrayOutputStream();
    final String oneMatchingETag =
        "\"cant_match\", \"wont_match\", \"no_match\", "
            + ETagCache.getInstance().getETag(targetFile);

    final HttpRequest httpRequest =
        new HttpRequest.Builder()
            .method(HttpMethod.GET.name())
            .version(HttpToken.SUPPORTED_HTTP_VERSION)
            .requestUri("/awesome.html")
            .addHeader(HttpHeader.IF_MATCH.getName(), oneMatchingETag)
            .build();
    final HttpIfMatchHandler ifMatchHandler = new HttpIfMatchHandler(httpRequest, rootDir, out);

    // when
    ifMatchHandler.handle();

    // then
    StringTokenizer tokenizer = new StringTokenizer(out.toString(), HttpToken.CARRIAGE_RETURN_LINE_FEED, false);
    assertEquals(tokenizer.nextToken(), "HTTP/1.1 200 OK");
  }

  @Test
  public void shouldContinueIfWildcardGivenAsETag() throws Exception {
    // given
    final ByteArrayOutputStream out = new ByteArrayOutputStream();
    final String wildCard = "*";
    final HttpRequest httpRequest =
        new HttpRequest.Builder()
            .method(HttpMethod.GET.name())
            .version(HttpToken.SUPPORTED_HTTP_VERSION)
            .requestUri("/awesome.html")
            .addHeader(HttpHeader.IF_MATCH.getName(), wildCard)
            .build();
    final HttpIfMatchHandler ifMatchHandler = new HttpIfMatchHandler(httpRequest, rootDir, out);

    // when
    ifMatchHandler.handle();

    // then
    StringTokenizer tokenizer = new StringTokenizer(out.toString(), HttpToken.CARRIAGE_RETURN_LINE_FEED, false);
    assertEquals(tokenizer.nextToken(), "HTTP/1.1 200 OK");
  }

}
