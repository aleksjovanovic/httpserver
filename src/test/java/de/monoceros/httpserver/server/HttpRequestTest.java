package de.monoceros.httpserver.server;

import de.monoceros.httpserver.constant.HttpMethod;
import de.monoceros.httpserver.constant.HttpToken;
import de.monoceros.httpserver.exception.HttpParseException;
import org.junit.Test;

import static org.junit.Assert.*;

public class HttpRequestTest {

  @Test
  public void shouldBuildValidHttpRequest() throws HttpParseException {
    // given
    HttpRequest.Builder builder = new HttpRequest.Builder();
    String requestUri = "/test";

    // when
    HttpRequest httpRequest =
        builder
            .method(HttpMethod.GET.name())
            .requestUri(requestUri)
            .version(HttpToken.SUPPORTED_HTTP_VERSION)
            .build();

    // then
    assertEquals(httpRequest.getRequestUri(), requestUri);
    assertEquals(httpRequest.getMethod(), HttpMethod.GET);
    assertEquals(httpRequest.getVersion(), HttpToken.SUPPORTED_HTTP_VERSION);
  }

  @Test(expected = HttpParseException.class)
  public void shouldThrowExceptionWhenMandatoryFieldsAreMissing() throws HttpParseException {
    // given
    HttpRequest.Builder builder = new HttpRequest.Builder();

    // when & then
    builder.build();
  }

  @Test(expected = HttpParseException.class)
  public void shouldThrowExceptionWhenMethodIsMissing() throws HttpParseException {
    // given
    HttpRequest.Builder builder = new HttpRequest.Builder();
    String requestUri = "/test";

    // when & then
    builder.requestUri(requestUri).version(HttpToken.SUPPORTED_HTTP_VERSION).build();
  }

  @Test(expected = HttpParseException.class)
  public void shouldThrowExceptionWhenVersionIsMissing() throws HttpParseException {
    // given
    HttpRequest.Builder builder = new HttpRequest.Builder();
    String requestUri = "/test";

    // when & then
    builder.requestUri(requestUri).method(HttpMethod.GET.name()).build();
  }

  @Test(expected = HttpParseException.class)
  public void shouldThrowExceptionWhenRequestUriIsMissing() throws HttpParseException {
    // given
    HttpRequest.Builder builder = new HttpRequest.Builder();

    // when & then
    builder.version(HttpToken.SUPPORTED_HTTP_VERSION).method(HttpMethod.GET.name()).build();
  }
}
